# WorldWotMap

Ceci est une carte affichant les comptes Ğ1 et leurs certifications. La carte peut être incomplète, car il est facultatif de renseigner sa position géographique sur Cesium+.

Voir la carte en ligne&nbsp;:  
https://zettascript.org/tux/g1/worldwotmap

Une partie de duniter.db est exportée en JSON par `gencache.py` (opération faite par le serveur). Les données Cesium+ sont chargées par le navigateur depuis une instance Cesium.

## Installation

Installer python3-plyvel&nbsp;:

	sudo pip3 install plyvel

Générez le cache JSON à partir de duniter.db (il vous faut un serveur Duniter arrêté pour cela)&nbsp;:

	python3 gencache.py -d /chemin/vers/duniter.db -e

WorldWotMap nécessite jQuery, D3, Leaflet et jLouvain. Modifiez les chemins dans le HTML en fonction de votre serveur.

 * https://leafletjs.com
 * https://jquery.com
 * https://github.com/upphiminn/jLouvain
 * https://d3js.org

C'est aussi plus joli avec une belle police, par exemple Proza Libre. https://fontlibrary.org/en/font/proza-libre

## Utilisation

Chaque point sur la carte représente un compte. Les points rouges sont les comptes certifiés, les points bleus sont des comptes non certifiés.

Chaque ligne reliant deux points rouges représente une certification.

Vous pouvez sélectionner une personne en cliquant sur son point. Les certifications qu'elle a reçues s'affichent en rouges, celles qu'elle a émises en vert. (si il y a un chevauchement entre une certification reçue et une émise, une seule couleur peut apparaître **(bug à corriger)**) Pour n'afficher que les certifications liées à cette personne, cochez la case "_Afficher uniquement ses certifications_".

Le menu en haut à droite dans la carte permet d'afficher ou de cacher certaines couches de données. La couche _Communautés_ regroupe les communautés de certifications par couleurs (avec l'algorithme Louvain).

Cherchez un compte en entrant sa clé publique en entier ou que le début, son titre Cesium+ ou son UID dans le champ de recherche.

### Paramètres GET

 * **lat**, **lon**&nbsp;: Latitude et longitude au centre de la carte (en degrés)
   * Zoom sur Bordeaux https://zettascript.org/tux/g1/worldwotmap.html?lat=44.82543879996824&lon=-0.5458831787109376&zoom=13  
   * Zoom sur Toulouse https://zettascript.org/tux/g1/worldwotmap.html?lat=43.595559991515586&lon=1.4560317993164065&zoom=12
 * **zoom**&nbsp;: Niveau de zoom
 * **a**&nbsp;: Terme de recherche pour le compte à sélectionner. Peut contenir une clé publique entière ou son début, le titre Cesium+ d'une identité ou son uid. Insensible à la casse.
   * https://zettascript.org/tux/g1/worldwotmap.html?a=rml12&popup#map
 * **only**&nbsp;: Nombre de niveaux de certifications à afficher autour du compte sélectionné. Une valeur numérique supérieure _0_ active ce mode.
   * https://zettascript.org/tux/g1/worldwotmap.html?lat=47.331377157798244&lon=2.9443359375&zoom=6&pubkey=45GfjkWCWQhJ3epJVGC2NSg1Rcu4Ue1vDD3kk9eLs5TQ&only=3#map
 * **popup**&nbsp;: Affiche initialement la popup sur le compte sélectionné avec _pubkey_.

## Contribuer

Dans le code&nbsp;:
 * Pouvoir représenter la distance d'un compte/d'une certification au compte sélectionné en faisant varier la couleur ou l'opacité.
 * Changer d'époque (afficher la wotmap à un temps donné) (à voir avec les données Cesium+)
 * Vérifier l'utilisation des paramètres GET et pseudos Ğ1 pour éviter le XSS
 * Les lignes dessinées dans le mode "afficher uniquement les certifications du compte sélectionné" ne s'effacent pas bien après déselection du compte.
 * Importer toutes les identités (actuellement, celles n'étant pas localisées sur Cesium+ ne sont pas importées). Cela permet d'importer toutes les certifications, donc de construire correctement l'arbre des certifications avec distance maximale ainsi que de trouver tous les membres référents.
 * Afficher des statistiques (nombre de certifs, moyenne par membre, données de distance géographique, découpages en régions par communautés géographiques ou de certifications)
 * Éventuellement créer un programme pour générer un JSON cache et éviter de surcharger les serveurs Cesium+.
 * Améliorer l'interface du menu&#8239;?
 * Choisir les couches à afficher dans les paramètres GET
 * Trouver un algo pour générer une liste de couleurs pas trop limitée pour les communautés

Sinon&nbsp;:
 * Héberger une instance chez vous et publier le lien
 * Proposer de l'aide pour la traduction (dans ce cas il faudra songer à implémenter un système de traduction) (après tout, c'est une carte du monde, pas que de la francophonie)
 * Suggestions&#8239;? (si vous n'avez pas de compte GitLab, contactez-moi sur Cesium ou le forum Duniter)

## License

GNU AGPL v3, CopyLeft 2020 Pascal Engélibert  
Inspired by [python-forceatlas2](https://bazaar.launchpad.net/~mwshinn/forceatlas2-python) (GNU GPL v3, CopyLeft 2016 Max Shinn <mws41@cam.ac.uk>)  
The ForceAtlas2 paper was released under CC BY, Copyright 2014 Jacomy et al.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/.
