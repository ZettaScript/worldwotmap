#!/usr/bin/env python3
"""
Dependances: python3-plyvel
Licence CC-0 (Public domain)
Compatible with Duniter >=1.7.9 (with LevelDB)
"""

import math, plyvel, sys, os, json

def getargv(argv, arg, default=""):
	if arg in argv and len(argv) > argv.index(arg)+1:
		return argv[argv.index(arg)+1]
	else:
		return default

def exportJSON(outfile, data):
	f = open(outfile, "w")
	json.dump(data, f)
	f.close()

if __name__ == "__main__":
	if "--help" in sys.argv:
		print("""Duniter DB extractor
Licence CC0
Duniter must not be running while extracting from LevelDB datas.

Options:
 -d <path>  Change duniter.db path
  default: ~/.config/duniter/duniter_default/data/leveldb
 -e <path>  Export db in JSON to <path>
  default: duniterdb.json
""")
		exit()
	
	result = {"certs":[], "ids":{}}
	
	dbpath = os.path.expanduser(getargv(sys.argv, "-d", "~/.config/duniter/duniter_default/data/leveldb"))
	iindex = plyvel.DB(dbpath + "/level_iindex")
	cindex = plyvel.DB(dbpath + "/level_cindex")
	
	i = 0
	for pub, row in iindex:
		idty = json.loads(row.decode())[0]
		certs = json.loads(cindex.get(pub).decode())
		
		result["ids"][idty["pub"]] = [idty["uid"], idty["pub"], idty["member"], idty["writtenOn"]]
		for cert in certs["issued"]:
			if cert["expired_on"] == 0:
				result["certs"].append([idty["pub"], cert["receiver"], cert["expires_on"], cert["expired_on"], cert["writtenOn"]])
		i += 1
	
	iindex.close()
	cindex.close()
	
	print("Identities: {}".format(i))
	
	exportJSON(getargv(sys.argv, "-e", "duniterdb.json"), result)
