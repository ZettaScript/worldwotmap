/*
	CopyLeft 2018-2020 Pascal Engélibert
	This file is part of WorldWotMap.

	WorldWotMap is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WorldWotMap is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WorldWotMap.  If not, see <https://www.gnu.org/licenses/>.
*/

// Instance settings
const SETTINGS = {
	"cesiumplus_url": "https://g1.data.duniter.fr/",
	"cesium_url": "https://g1.duniter.fr/",
	"duniterdb_url": "data/duniterdb.json",
	"maptiles_url": "https://{s}.tile.osm.org/{z}/{x}/{y}.png"
};

var map_tiles_attribution = '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors';
var map_default_pos = [46.725, 2.834];
var map_default_zoom = 6;

class Account {
	constructor(pubkey, title, pos, address, city, point, avatar, community=null) {
		this.pubkey = pubkey;
		this.title = title;
		this.pos = pos;
		this.address = address;
		this.city = city;
		this.point = point;
		this.certsIssued = [];
		this.certsReceived = [];
		this.avatar = avatar;
		this.community = community;
	}
	
	mapActiveAccount() {// `this` is the Leaflet Circle, not the Account
		var account = accounts[this.options["pubkey"]];
		if(active_account != null && active_account != account.pubkey)
			mapInactiveAccount(active_account);
		active_account = account.pubkey;
		onlyShowCertsOfSelected()
		$("#mapmenu-idcard").attr("style", "display: initial;");
		$("#mapmenu-idcard-bt").attr("onclick", "mapInactiveAccount('"+active_account+"');");
		$("#mapmenu-idcard .idcard-title").html(account.title);
		$("#mapmenu-idcard .idcard-pubkey").html(active_account);
		$("#mapmenu-idcard .idcard-pubkey-short").html(active_account.substr(0,8));
		if(account.avatar) {
			$("#mapmenu-idcard .idcard-avatar").attr("src", settings["cesiumplus_url"]+"user/profile/"+active_account+"/_image/avatar.png");
			$("#mapmenu-idcard .idcard-avatar").attr("style", "display: initial;");
		} else {
			$("#mapmenu-idcard .idcard-avatar").attr("src", "");
			$("#mapmenu-idcard .idcard-avatar").attr("style", "display: none;");
		}
		if(account.community != null) {
			$("#mapmenu-idcard .idcard-community").html("Communauté <strong>"+account.community+"</strong>");
			$("#mapmenu-idcard .idcard-community").attr("style", "border-left:1em solid "+communities[account.community].color+";");
		} else {
			$("#mapmenu-idcard .idcard-community").html("");
			$("#mapmenu-idcard .idcard-community").attr("style", "");
		}
		$("#mapmenu-idcard .idcard-ncerts-received").html(account.certsReceived.length);
		$("#mapmenu-idcard .idcard-ncerts-issued").html(account.certsIssued.length);
		$("#mapmenu-idcard-cesiumlink").attr("href", settings["cesium_url"]+"#/app/wot/"+active_account+"/");
		for(var cert in account.certsIssued) {
			certs[account.certsIssued[cert]].line.setStyle({color:"#4f4", weight:3, opacity:0.6});
		}
		for(var cert in account.certsReceived) {
			certs[account.certsReceived[cert]].line.setStyle({color:"#f44", weight:3, opacity:0.6});
		}
		showURL();
	}
}

class Cert {
	constructor(issuer, receiver, line) {
		this.issuer = issuer;
		this.receiver = receiver;
		this.line = line;
	}
}

function mapInactiveAccount(pubkey) {
	if(accounts[active_account].point.isPopupOpen())
		accounts[active_account].point.closePopup();
	active_account = null;
	onlyShowCertsOfSelected()
	var account = accounts[pubkey];
	$("#mapmenu-idcard").attr("style", "display: none;");
	$("#mapmenu-idcard").attr("onclick", "");
	$("#mapmenu-idcard .idcard-title").html("");
	$("#mapmenu-idcard .idcard-pubkey").html("");
	$("#mapmenu-idcard .idcard-pubkey-short").html();
	$("#mapmenu-idcard .idcard-avatar").attr("style", "display: none;");
	$("#mapmenu-idcard .idcard-community").html("");
	$("#mapmenu-idcard .idcard-community").attr("style", "");
	$("#mapmenu-idcard-cesiumlink").attr("href", "#");
	for(var cert in account.certsIssued) {
		certs[account.certsIssued[cert]].line.setStyle({color:"black", weight:1, opacity:0.3});
	}
	for(var cert in account.certsReceived) {
		certs[account.certsReceived[cert]].line.setStyle({color:"black", weight:1, opacity:0.3});
	}
	onlyShowCertsOfSelected();
	showURL();
}

// mode: true=received / false=issued
function getAccountsWithDistance(pubkey, distance, mode) {
	var c = [];// certs
	var a = {};// accounts
	a[pubkey] = 0;
	
	var d = 0;
	var as = {0:pubkey};// accounts stack
	var cs = {0:[]};// certs stack
	
	if(mode)
		cs[0] = accounts[pubkey].certsReceived.slice();
	else
		cs[0] = accounts[pubkey].certsIssued.slice();
	
	var i = 0;
	while(cs[0].length > 0 && i < 10000) {
		while(!(d in cs) || cs[d].length == 0 || d >= distance) {
			d --;
		}
		var cert = cs[d].pop();
		d ++;
		c.push(cert);
		if(mode) {
			if(!(certs[cert].issuer in a) || d < a[certs[cert].issuer]) {
				a[certs[cert].issuer] = d;
				as[d] = certs[cert].issuer;
				cs[d] = accounts[certs[cert].issuer].certsReceived.slice();
			}
		}
		else {
			if(!(certs[cert].receiver in a) || d < a[certs[cert].receiver]) {
				a[certs[cert].receiver] = d;
				as[d] = certs[cert].receiver;
				cs[d] = accounts[certs[cert].receiver].certsIssued.slice();
			}
		}
		i ++;
	}
	return [a, c];
}

function onlyShowCertsOfSelected() {
	for(var cert in active_certs) {
		active_certs[cert][1].remove();
	}
	active_certs = [];
	if(active_account == null || !document.getElementById("mapmenu-idcard-onlycertsofsel").checked) {
		p_certlines.style.display = "initial";
		return;
	}
	p_certlines.style.display = "none";
	
	var show_certs = [];
	if(document.getElementById("mapmenu-idcard-onlycertsofsel-t").value == "all" ||
	   document.getElementById("mapmenu-idcard-onlycertsofsel-t").value == "received") {
		show_certs = show_certs.concat(getAccountsWithDistance(active_account, document.getElementById("mapmenu-idcard-onlycertsofsel-n").value, true)[1]);
	}
	if(document.getElementById("mapmenu-idcard-onlycertsofsel-t").value == "all" ||
	   document.getElementById("mapmenu-idcard-onlycertsofsel-t").value == "issued") {
		show_certs = show_certs.concat(getAccountsWithDistance(active_account, document.getElementById("mapmenu-idcard-onlycertsofsel-n").value, false)[1]);
	}
	for(var cert in show_certs) {
		var color = "black";
		if(certs[show_certs[cert]].issuer == active_account)
			color = "#4f4";
		else
			color = "#f44";
		var line = L.polyline([accounts[certs[show_certs[cert]].issuer].pos, accounts[certs[show_certs[cert]].receiver].pos], {color:color, weight:2, opacity:0.7}).addTo(map);
		line.bindPopup("<b>"+accounts[certs[show_certs[cert]].issuer].title+"</b> &rarr; <b>"+accounts[certs[show_certs[cert]].receiver].title+"</b>");
		line.addTo(l_certlines);
		active_certs.push([show_certs[cert], line]);
		active_ncerts ++;
	}
}

function showURL() {
	var pos = map.getCenter();
	var url = window.location.pathname + "?lat="+pos.lat+"&lon="+pos.lng+"&zoom="+map.getZoom();
	if(active_account != null)
		url += "&a="+active_account;
	if(document.getElementById("mapmenu-idcard-onlycertsofsel").checked)
		url += "&only="+document.getElementById("mapmenu-idcard-onlycertsofsel-n").value;
	$("#mapmenu-maplink").attr("href", url+"#map");
}

function search(term, goto=true, popup=true, title=true) {
	term = term.trim();
	var obj = document.getElementById("mapmenu-searchresults");
	obj.innerHTML = "";
	if(term == "")
		return 0;
	if(term in accounts) {// lazy
		active_account = term;
		map.setView(accounts[active_account].pos);
		if(popup)
			accounts[active_account].point.openPopup();
		else
			accounts[active_account].point.fire("popupopen");
		alertOnMap(accounts[active_account].pos);
		return 1;
	}
	// zealous
	var results = [];
	var n = 0;
	if(term.match(/^[0-9a-z]{1,43}$/i)) {
		var re = new RegExp("^"+term, "i");// starting by
		for(a in accounts) {
			if(a.match(re)) {
				results.push(a);
				n ++;
				if(n > 20)// too many results
					break;
			}
		}
	}
	if(title) {
		var re = new RegExp(term, "i");
		for(a in accounts) {
			var positive = accounts[a].title.match(re);
			if(!positive && a in ids) {
				if(ids[a][0] != null)
					positive = ids[a][0].match(re);
			}
			if(positive && results.indexOf(a) == -1) {
				results.push(a);
				n ++;
				if(n > 20)// too many results
					break;
			}
		}
	}
	if(n == 1 && goto) {
		active_account = results[0];
		map.setView(accounts[active_account].pos);
		if(popup)
			accounts[active_account].point.openPopup();
		else
			accounts[active_account].point.fire("popupopen");
		alertOnMap(accounts[active_account].pos);
	}
	else {
		var htmlresults = "";
		for(result in results) {
			var member = (results[result] in ids && ids[results[result]][0] != null) ? ids[results[result]][0] : null;
			htmlresults += '<div class="searchresult" onclick="search(\''+results[result]+'\','+popup+');" title="'+accounts[results[result]].pubkey+(member?" / "+member:"")+'">'+accounts[results[result]].title+(member?' <span class="membername">'+member+'</span>':"")+'</div>';
		}
		obj.innerHTML = htmlresults;
	}
}

function alertOnMap(pos, radius=64, color="red", opacity=0.3) {
	var point = L.circle(pos, {
		color: color,
		fillColor: color,
		fillOpacity: opacity,
		radius: pixelsToMeters(pos[0], radius, map.getZoom())
	}).addTo(map);
	setTimeout(function(){point.removeFrom(map);}, 500);
}

var settings = {};
Object.assign(settings, SETTINGS);
loadSettings();

var communities = {};
var ids = {};
var accounts = {};
var certs = [];
var ncerts = 0;
var active_certs = [];
var active_ncerts = [];
var active_account = null;
var map = L.map("map").setView([getParam("lat",map_default_pos[0]), getParam("lon",map_default_pos[1])], getParam("zoom",map_default_zoom));
L.tileLayer(settings["maptiles_url"], {attribution: map_tiles_attribution}).addTo(map);
var p_certlines = map.createPane("certlines");
var l_accounts_cert = L.layerGroup();
var l_accounts_wallet = L.layerGroup();
var l_certlines = L.layerGroup([]);
var l_communities = L.layerGroup([]);
var overlayMaps = {
	"Identités": l_accounts_cert,
	"Porte-feuilles": l_accounts_wallet,
	"Certifications": l_certlines,
	"Communautés": l_communities
};
L.control.layers({}, overlayMaps).addTo(map);
L.control.scale().addTo(map);
map.on("moveend", showURL);
map.on("zoomend", showURL);

$.post(settings["cesiumplus_url"]+"user/profile/_search", '{"query":{"bool":{"must":[{"exists":{"field":"geoPoint"}}]}},"from":0,"size":0,"_source":[]}', function(data) {
	$("#mapmenu-naccounts").html(data["hits"]["total"]+" comptes trouvés");
	$.post(settings["cesiumplus_url"]+"user/profile/_search", '{"query":{"bool":{"must":[{"exists":{"field":"geoPoint"}}]}},"from":0,"size":'+data["hits"]["total"]+',"_source":["title","geoPoint","avatar._content_type","address","city","description"]}', function(data) {
		var accounts_data = data["hits"]["hits"];
		for(var i=0; i < accounts_data.length; i ++) {
			var account_data = accounts_data[i];
			var point = L.circle([account_data["_source"]["geoPoint"]["lat"], account_data["_source"]["geoPoint"]["lon"]], {
				color: "#08f",
				fillColor: "#08f",
				fillOpacity: 0.1,
				radius: 50,
				pubkey: account_data["_id"]// Yay! Hack because JS is so bad with pointers...
			}).addTo(map);
			point.bindPopup("<b>"+account_data["_source"]["title"]+"</b><br/>"+account_data["_id"]);
			var account = new Account(account_data["_id"], account_data["_source"]["title"], [account_data["_source"]["geoPoint"]["lat"], account_data["_source"]["geoPoint"]["lon"]], account_data["_source"]["address"], account_data["_source"]["city"], point, "avatar" in account_data["_source"]);
			accounts[account_data["_id"]] = account;
			point.on("popupopen", account.mapActiveAccount);
			point.addTo(l_accounts_wallet);
		}
		
		$.get(settings["duniterdb_url"], null, function(data) {
			ids = data["ids"];
			
			// Add certs
			for(var i=0; i < data["certs"].length; i ++) {
				if(data["certs"][i][0] in accounts && data["certs"][i][1] in accounts && data["certs"][i][3] == 0) {
					var line = L.polyline([accounts[data["certs"][i][0]].pos, accounts[data["certs"][i][1]].pos], {color:'black', weight:1, opacity:0.3, pane:"certlines"}).addTo(map);
					line.bindPopup("<b>"+accounts[data["certs"][i][0]].title+"</b> &rarr; <b>"+accounts[data["certs"][i][1]].title+"</b><br />"+round(geoDist(accounts[data["certs"][i][0]].pos, accounts[data["certs"][i][1]].pos),2)+" km");
					line.addTo(l_certlines);
					certs.push(new Cert(data["certs"][i][0], data["certs"][i][1], line));
					accounts[data["certs"][i][0]].certsIssued.push(ncerts);
					accounts[data["certs"][i][1]].certsReceived.push(ncerts);
					ncerts ++;
				}
			}
			l_certlines.addTo(map);
			
			// Change point colors
			for(id in data["ids"]) {
				if(id in accounts) {
					if(accounts[id].certsIssued.length >= 5 && accounts[id].certsReceived.length >= 5) {
						accounts[id].point.setStyle({color: "#0c0", fillColor: "#0c0"});
					}
					else {
						accounts[id].point.setStyle({color: "red", fillColor: "red"});
					}
					accounts[id].point.removeFrom(l_accounts_wallet);
					accounts[id].point.addTo(l_accounts_cert);
				}
			}
			l_accounts_cert.addTo(map);
			l_accounts_wallet.addTo(map);
			
			// Select account given in URL
			var url_account = getParam("a");
			if(url_account != null) {
				if(search(url_account, true, getParam("popup")) == 1) {
					if(getParam("only", 0) > 0) {
						$("#mapmenu-idcard-onlycertsofsel").attr("checked", "checked");
						$("#mapmenu-idcard-onlycertsofsel-n").attr("value", getParam("only"));
						onlyShowCertsOfSelected();
					}
				}
			}
			
			// Start communities detection
			computeCommunities();
		});
	});
});

// Compute communities
class CommunityEdge {
	constructor(source, target, weight=1) {
		this.source = source;
		this.target = target;
		this.weight = weight;
	}
}

class Community {
	constructor(color) {
		this.color = color;
	}
}

function computeCommunities(circles=false) {
	var colors = ["#ff0000", "#00ff00", "#0000ff", "#c0c000", "#c000c0", "#00c0c0", "#d05000", "#d00050", "#50d000", "#00d050", "#d00050", "#00d050"];
	var _nodes = [];
	var _edges = [];
	
	for(id in ids) {
		_nodes.push(id);
	}
	for(cert in certs) {
		_edges.push(new CommunityEdge(certs[cert].issuer, certs[cert].receiver));
	}
	
	var _communities = jLouvain().nodes(_nodes).edges(_edges)();	
	var _points = {};
	var _allpoints = [];
	var i = 0;
	for(a in _communities) {
		if(a in accounts) {
			if(!(_communities[a] in communities)) {
				communities[_communities[a]] = new Community(colors[i % colors.length]);
				_points[_communities[a]] = [];
				i ++;
			}
			accounts[a].community = _communities[a];
			var point = L.circle(accounts[a].pos, {
				color: communities[_communities[a]].color,
				fillColor: communities[_communities[a]].color,
				fillOpacity: 0.2,
				radius: 100,
				pubkey: a
			}).addTo(map);
			point.addTo(l_communities);
			if(accounts[a].pos != null) {
				_allpoints.push(a);
				_points[_communities[a]].push(a);
			}
		}
	}
	l_communities.addTo(map);
	l_communities.remove(map);
	
	$("#mapmenu-ncommunities").html(i + " communautés");
	
	var voronoi = d3.voronoi()
	  .x(function(a) { return accounts[a].pos[0]; })
	  .y(function(a) { return accounts[a].pos[1]; });
	
	var _polygons = voronoi(_allpoints).polygons();
	for(_poly in _polygons) {
		if(_polygons[_poly].indexOf(null) == -1)
			L.polygon(_polygons[_poly], {stroke: false, fillOpacity: 0.3, color: communities[accounts[_polygons[_poly].data].community].color}).addTo(l_communities);
		else
			console.log(_polygons[_poly]);
	}
}
