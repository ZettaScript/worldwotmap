// -------- Maths

function rad(deg) {
	return deg * Math.PI / 180;
}

function geoDist(pos1, pos2) {// https://stackoverflow.com/questions/365826/calculate-distance-between-2-gps-coordinates
	var a = Math.sin(rad(pos2[0]-pos1[0])/2)**2 + Math.sin(rad(pos2[1]-pos1[1])/2)**2 * Math.cos(rad(pos1[0])) * Math.cos(rad(pos2[0]));
	return 12742 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
}

function round(value, decimals) {// http://www.jacklmoore.com/notes/rounding-in-javascript/
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

function metersPerPixel(lat, zoom) {
	return 40075017 * Math.cos(rad(lat)) / Math.pow(2, zoom + 8);
}

function metersToPixels(lat, meters, zoom) {
	return meters / metersPerPixel(lat, zoom);
}

function pixelsToMeters(lat, pixels, zoom) {
	return pixels * metersPerPixel(lat, zoom);
}

// -------- URL

function getParam(param, def=null) {
	var result = def, tmp = [];
	var items = location.search.substr(1).split("&");
	for(var i = 0; i < items.length; i++) {
		tmp = items[i].split("=");
		if(tmp[0] === param) result = decodeURIComponent(tmp[1]);
	}
	return result;
}

// -------- Cookies

function getCookie(name) {
	const value = `; ${document.cookie}`;
	const parts = value.split(`; ${name}=`);
	if(parts.length === 2) return parts.pop().split(";").shift();
}

function setCookie(name, val, expires="", path="", domain="") {
	document.cookie = name+"="+val+";path="+path+";domain="+domain+";expires="+expires;
}

function deleteCookie(name, path="", domain="") {
	if(getCookie(name))
		document.cookie = name+"=;path="+path+";domain="+domain+";expires=Thu, 01 Jan 1970 00:00:01 GMT";
}

// -------- Settings

function loadSettings() {
	console.log(settings);
	for(setting in settings) {
		let val = getCookie(setting);
		console.log(setting+" = "+val);
		if(val)
			settings[setting] = val;
		document.getElementById("settings-"+setting).value = settings[setting];
	}
	
	console.log(settings);
}

function saveSettings() {
	for(setting in settings) {
		let val = document.getElementById("settings-"+setting).value;
		if(val == "") {
			settings[setting] = SETTINGS[setting];
			deleteCookie(setting);
		}
		else if(val != settings[setting]) {
			settings[setting] = val;
			if(val == SETTINGS[setting])
				deleteCookie(setting);
			else
				setCookie(setting, val);
		}
	}
}
